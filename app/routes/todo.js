import Route from '@ember/routing/route';

export default class TodoRoute extends Route {
  model() {
    return [
      {
        id: 1,
        todo: 'Check in',
      },
      {
        id: 2,
        todo: 'Brief',
      },
      {
        id: 3,
        todo: 'Review',
      },
    ];
  }
}
